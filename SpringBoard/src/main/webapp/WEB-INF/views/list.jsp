<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Bootstrap 3, from LayoutIt!</title>

<meta name="description"
	content="Source code generated using layoutit.com">
<meta name="author" content="LayoutIt!">

<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">

</head>
<body>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				${startPage} / ${endPage}
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th>번호</th>
							<th>제목</th>
							<th>내용</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="obj" items="${list}">

							<tr>
								<td>${obj.B_ID}</td>
								<td>${obj.B_TITLE}</td>
								<td>${obj.B_CONTENT}</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="pagination">
					<li><a href="list.do?page=${startPage - 10}">Prev</a></li>
					<c:forEach var="i" begin="${startPage}" end="${endPage}">
						<li><a href="list.do?page=${i}">${i}</a></li>
					</c:forEach>
					<li><a href="list.do?page=${startPage + 10}">Next</a></li>
				</ul>
			</div>
		</div>
	</div>

	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/scripts.js"></script>
</body>
</html>







