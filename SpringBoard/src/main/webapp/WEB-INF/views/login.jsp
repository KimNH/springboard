<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
<script>
	$(document).ready(function() {
		$("input[type=submit]").click(function() {
			$.ajax({
				url : "login.do",
				type : "post",
				data : $("form").serialize(),
				success : function(data) {
					alert(data.msg);
				},
				error : function() {
					alert("서버 오류");
				}
			});
			
			return false;
		});
	});
</script>
</head>
<body>
	${sessionScope.id}님 로그인
	<form method="post">
		ID : <input type="text" name="id">
		<br>
		PW : <input type="text" name="pw">
		<br>
		<input type="submit" value="로그인">
	</form>
</body>
</html>



