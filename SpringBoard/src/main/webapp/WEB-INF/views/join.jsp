<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
<script>
	$(document).ready(function() {
		$("input:submit").click(function() {
			
			// Asynchronos Javascript And Xml
			$.ajax({
				url : "join.do",
				type : "post",
				data : $("form").serialize(), // id=a&pw=1&name=a1
				success : function(result) {
					alert(result.msg);
					alert(result.code);
				}
			});
			
			return false;
		});
		$("#check").click(function() {
			var id = $("input[name=id]").val();
			
			$.ajax({
				url : "idCheck.do?id=" + id,
				type : "get",
				data : {},
// 				data : {"id" : id},
				success : function(result) {
					alert(result.msg);
				}
			});
			
			return false;
		});
	});
</script>
</head>
<body>
	<form method="post">
		ID : <input type="text" name="id">
		<button id="check">중복확인</button>
		<br>
		PW : <input type="text" name="pw">
		<br>
		이름 : <input type="text" name="name">
		<br>
		<input type="submit" value="가입하기">
	</form>
</body>
</html>

