package com.spring.board.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDao {
	@Autowired
	JdbcTemplate jdbc;
	
	public String selectByIdForLogin(String id) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT PW");
		sql.append("  FROM SPRING_MEMBER");
		sql.append(" WHERE ID = ?");
		
		Map<String, Object> map = null;
		String dbPw = "";
		// 조회결과가 없는 경우 예외처리
		try {
			map = jdbc.queryForMap(sql.toString(), id);
			dbPw = (String) map.get("PW");
		} catch(EmptyResultDataAccessException e) {
			
		}
		
		return dbPw;
	}
	
	public String selectById(String id) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ID");
		sql.append("  FROM SPRING_MEMBER");
		sql.append(" WHERE ID = ?");
		
		
		Map<String, Object> map = null;
		String dbId = "";
		// 조회결과가 없는 경우 예외처리
		try {
			map = jdbc.queryForMap(sql.toString(), id);
			dbId = (String) map.get("ID");
		} catch(EmptyResultDataAccessException e) {
			
		}
		
		return dbId;
	}
	
	public int insert(String id, String pw, String name) {
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO SPRING_MEMBER");
		sql.append("       (ID, PW, NAME)");
		sql.append("    VALUES");
		sql.append("       (?, ?, ?)");
		
		int result = jdbc.update(sql.toString(), id, pw, name);
		return result;
	}
}













