package com.spring.board.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.board.dao.MemberDao;

@Service
public class MemberService {
	@Autowired
	MemberDao mDao;
	
	public String login(String id) {
		String dbPw = mDao.selectByIdForLogin(id);
		return dbPw;
	}
	
	public String checkId(String id) {
		String dbId = mDao.selectById(id);
		return dbId;
	}
	
	public void addMember(Map<String, String> map) {
		mDao.insert(
			map.get("id"), map.get("pw"), map.get("name"));
	}
}












