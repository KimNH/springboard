package com.spring.board.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.spring.board.service.BoardService;

@Controller
public class BoardController {
	@Autowired
	BoardService bService;
	
	@RequestMapping(value="upload.do", method=RequestMethod.GET)
	public String uploadForm() {
		return "images_upload";
	}
	@RequestMapping(value="upload.do", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> upload(
			MultipartHttpServletRequest mReq) {
		
		MultipartFile mFile = mReq.getFile("file");
		String fileName = mFile.getOriginalFilename();
		
		// xxxxxxxxxxxxxxxxxxxxxxx/webapp/WEB-INF/classes
		String webAppPath = 
			this.getClass().getClassLoader().getResource("").getPath();
		int idx = webAppPath.indexOf("/WEB-INF");
		webAppPath = webAppPath.substring(0, idx);
		// xxxxxxxxxxxxxxxxxxxxxxx/webapp/resources/image
		webAppPath = webAppPath + "/resources/image/";
		
		File dir = new File(webAppPath);
		if(!dir.isDirectory()) {
			dir.mkdirs(); // 디렉토리 생성
		}
		
		
		try {
			mFile.transferTo(new File(webAppPath + fileName));
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Map<String, Object> returnMap =
				new HashMap<String, Object>();
		
		returnMap.put("filelink", "resources/image/" + fileName);
		
		return returnMap;
	}
	
	@RequestMapping("list.do")
	public ModelAndView list(
		@RequestParam(
			name="page", defaultValue="1", required=false) int page) {
		
		int startRow = 0; // 게시물 시작번호
		int endRow = 0; // 게시물 마지막번호
		
		endRow = page * 10;
		startRow = endRow - 10;
		
		int totalCount = bService.getTotalCount();
		
		int startPage = page / 10 * 10;
		if(page % 10 == 0) {
			startPage = startPage - 9;
		} else {
			startPage += 1;
		}
		int endPage = startPage + 9;
		int totalPage = totalCount / 10;
		if(totalCount % 10 != 0) {
			totalPage += 1;
		}
		
		if(endPage > totalPage) {
			endPage = totalPage;
		}
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("list");
		mav.addObject("startPage", startPage);
		mav.addObject("endPage", endPage);
		mav.addObject("list", bService.getBoardList(startRow));
		return mav;
	}
	
//	@RequestMapping("list.do")
//	public String list(HttpServletRequest request, Model model) {
//		Map<String, Object> returnMap = new HashMap<String, Object>();
//		returnMap.put("list", bService.getBoardList());
//		ObjectMapper om = new ObjectMapper();
//		try {
//			String json = om.writeValueAsString(returnMap);
//			request.setAttribute("data", json);
//			model.addAttribute("data2", json);
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//		}
//		return "list";
//	}
	
	
//	@RequestMapping("list.do")
//	@ResponseBody
//	public Map<String, Object> list() {
//		Map<String, Object> returnMap = new HashMap<String, Object>();
//		returnMap.put("list", bService.getBoardList());
//		return returnMap;
//	}
}








