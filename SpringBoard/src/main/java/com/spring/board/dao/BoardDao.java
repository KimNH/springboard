package com.spring.board.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BoardDao {
	@Autowired
	JdbcTemplate jdbc;
	
	public Integer getTotalCount() {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(*)");
		sql.append("  FROM SPRING_BOARD");
		
		return jdbc.queryForObject(sql.toString(), Integer.class);
	}
	public List<Map<String, Object>> select(int startRow) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT B_ID, B_TITLE, B_CONTENT");
		sql.append("  FROM SPRING_BOARD");
		sql.append(" LIMIT ?, 10");
		
		return jdbc.queryForList(sql.toString(), startRow);
	}
}













