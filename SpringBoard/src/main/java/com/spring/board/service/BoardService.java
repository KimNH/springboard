package com.spring.board.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.board.dao.BoardDao;

@Service
public class BoardService {
	@Autowired
	BoardDao bDao;

	public int getTotalCount() {
		return bDao.getTotalCount();
	}
	public List<Map<String, Object>> getBoardList(int startRow) {
		return bDao.select(startRow);
	}
}











