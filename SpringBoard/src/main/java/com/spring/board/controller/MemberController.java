package com.spring.board.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.board.service.MemberService;

@Controller
public class MemberController {
	@Autowired
	MemberService mService;
	
	@RequestMapping(value="login.do", method=RequestMethod.GET)
	public String loginForm() {
		return "login";
	}
	
	@RequestMapping(value="login.do", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> login(
			@RequestParam Map<String, String> map,
			HttpSession session) {
		String id = map.get("id");
		
		String dbPw = mService.login(id);
		
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		if(dbPw.equals("")) { // ID 없음
			returnMap.put("msg", "ID를 확인해주세요.");
		} else { // ID 있음
			String pw = map.get("pw");
			if(pw.equals(dbPw)) { // PW 맞음, 로그인 성공
				returnMap.put("msg", "로그인 되었습니다.");
				session.setAttribute("id", id);
			} else { // ID는 맞지만 PW 틀림
				returnMap.put("msg", "PW를 확인해주세요.");
			}
		}
		
		return returnMap;
	}
	
	
	
	@RequestMapping("idCheck.do")
	@ResponseBody
	public Map<String, Object> idCheck(
			@RequestParam("id") String id) {
		
		String dbId = mService.checkId(id);
		String msg = "";
		if(dbId.equals("")) {
			msg = "사용 가능한 ID 입니다.";
		} else {
			msg = "이미 사용중인 ID 입니다.";
		}
		
		Map<String, Object> returnMap =
				new HashMap<String, Object>();
		returnMap.put("msg", msg);
		
		return returnMap;
	}
	
	@RequestMapping(value="join.do", method=RequestMethod.GET)
	public String joinForm() {
		return "join";
	}
	
	@RequestMapping(value="join.do", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> join(@RequestParam Map<String, String> map) {
		System.out.println(map);
		
		mService.addMember(map);
		
		Map<String, Object> returnMap =
				new HashMap<String, Object>();
		returnMap.put("msg", "success");
		returnMap.put("code", 200);
		
		return returnMap;
	}

	@RequestMapping(value="test.do", method=RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> test(
			@RequestParam Map<String, String> map) {
		Map<String, Object> returnMap =
				new HashMap<String, Object>();
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("B");
		returnMap.put("list", list);
		returnMap.put("msg", "success");
		returnMap.put("code", 200);
		
		return returnMap;
	}

	@Autowired
	JdbcTemplate jdbc;
	
	@RequestMapping("insert.do")
	public void insert() {
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO SPRING_BOARD");
		sql.append("        VALUES");
		sql.append("       (NULL, ?, ?)");
		
		for(int i = 0; i < 300; i++) {
			jdbc.update(sql.toString(), "A", "A" + i);
		}
	}
	
	
}

























